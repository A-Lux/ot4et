<?php


namespace App\Domain\Contracts;


abstract class MainContract
{
    const ID    =   'id';
    const NAME  =   'name';
    const EMAIL =   'email';
    const USER  =   'user';
    const TOKEN =   'token';
    const SUM   =   'sum';
    const PRICE =   'price';
    const BRAND =   'brand';
    const COLOR =   'color';
    const DEAL  =   'deal';
    const DATE  =   'date';
    const ORDER =   'order';
    const TEXT  =   'text';
    const DESC  =   'desc';
    const NEWS  =   'news';
    const USERS =   'users';
    const UUID  =   'uuid';
    const QUEUE =   'queue';
    const VALUE =   'value';
    const ECP   =   'ecp';

    const ABILITIES =   'abilities';
    const TOKENABLE =   'tokenable';
    const FAILED_AT =   'failed_at';
    const EXCEPTION =   'exception';
    const PAYLOAD   =   'payload';
    const ADDRESS   =   'address';
    const QUESTION  =   'question';
    const QUESTIONS =   'questions';
    const CONTENT   =   'content';
    const PRODUCT   =   'product';
    const PRODUCTS  =   'products';
    const INVOICE   =   'invoice';
    const AMOUNT    =   'amount';
    const MESSAGE   =   'message';
    const BARCODE   =   'barcode';
    const SORT_TYPE =   'sort_type';
    const SEARCH    =   'search';
    const USER_ID   =   'user_id';
    const APP_NAME  =   'APP_NAME';
    const SALARY    =   'salary';
    const ROLE_ID   =   'role_id';
    const LAST_NAME =   'last_name';
    const PASSWORD  =   'password';
    const QUANTITY  =   'quantity';
    const INVOICES  =   'invoices';
    const SERVICES  =   'services';
    const SERVICE   =   'service';
    const TMP_NAME  =   'tmp_name';

    const SERVICE_ID    =   'service_id';
    const PRODUCT_ID    =   'product_id';
    const SHORT_NAME    =   'short_name';
    const LAST_USED_AT  =   'last_used_at';
    const CONNECTION    =   'connection';
    const CREATED_AT    =   'created_at';
    const ORGANIZATION  =   'organization';
    const MAIN_IMAGE    =   'main_image';
    const DIMENSIONS    =   'dimensions';
    const PARTNER_ID    =   'partner_id';
    const CERTIFICATE   =   'certificate';
    const IS_PRODUCT    =   'is_product';
    const DIMENSION_ID  =   'dimension_id';
    const CERTIFICATES  =   'certificates';
    const SORT_VALUE    =   'sort_value';
    const CURRENCY_ID   =   'currency_id';
    const FIRST_NAME    =   'first_name';
    const ORDER_NUMBER  =   'order_number';
    const TOTAL_AMOUNT  =   'total_amount';
    const PRODUCIBLE_ID =   'producible_id';

    const SERVICEABLE_TYPE  =   'serviceable_type';
    const SERVICEABLE_ID    =   'serviceable_id';
    const PRODUCIBLE_TYPE   =   'producible_type';
    const PAYMENT_ACCOUNT   =   'payment_account';
    const DOCUMENT_NUMBER   =   'document_number';
    const ORGANIZATION_ID   =   'organization_id';
    const REMEMBER_TOKEN    =   'remember_token';
    const EMAIL_VERIFIED_AT =   'email_verified_at';

    const IDENTIFICATION_NUMBER =   'identification_number';
    const DATE_OF_CONSTRUCTING  =   'date_of_constructing';

    const CALCULATION_COEFFICIENT   =   'calculation_coefficient';
    const GOVERNMENT_REVENUE_CODE   =   'government_revenue_code';

    const BUSINESS_IDENTIFICATION_NUMBER    =   'business_identification_number';
    const GOVERNMENT_REVENUE_CODE_BY_PLACE  =   'government_revenue_code_by_place';
}
