<?php

namespace App\Http\Controllers\Api\Auth;


use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use App\Domain\Contracts\UserContract;

class RegisterController extends Controller
{
    protected $userService;
    public function __construct(UserService $userService)
    {
        $this->userService  =   $userService;
    }

    public function register(RegisterRequest $request)
    {
        $user   =   $this->userService->create($request->all());
        $token  =   $user->createToken(env(UserContract::APP_NAME));
        return response([
            UserContract::USER  =>  new UserResource($user),
            UserContract::TOKEN =>  $token->plainTextToken
        ]);
    }
}
